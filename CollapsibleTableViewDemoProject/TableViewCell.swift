//
//  TableViewCell.swift
//  CollapsibleTableViewDemoProject
//
//  Created by Rukmani on 20/09/17.
//  Copyright © 2017 Rukmani. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {


    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var arrowLabel: UILabel!
    
    var isExpandable: Bool = false
    var items = [String]()
    var tapDelegate: ToggleHandler?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(title: String, isExpandable: Bool, tapDelegate: ToggleHandler) {
        self.cellLabel.text = title
        self.isExpandable = isExpandable
        if isExpandable {
            self.arrowLabel.isHidden = true
        } else {
            self.arrowLabel.isHidden = false
        }
    }
    
    func setCollapsed(isCollapsed: Bool) {
        if isCollapsed {
            arrowLabel.rotate(0.0)
        } else {
            arrowLabel.rotate(CGFloat(Double.pi/2))
        }
    }
    
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        self.addGestureRecognizer(tapGesture)
    }
    
    func handleTap() {
        self.tapDelegate?.handleToggle(sender: self, section: self.tag)
    }

}
