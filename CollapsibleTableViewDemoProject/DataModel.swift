//
//  DataModel.swift
//  CollapsibleTableViewDemoProject
//
//  Created by Rukmani on 20/09/17.
//  Copyright © 2017 Rukmani. All rights reserved.
//

import Foundation

struct Category {
    var categoryName: String
    var subCategories: [SubCategory]?
    var isCollapsed: Bool
    var isCollapsible: Bool
    
    init(categoryName: String, subCategories: [SubCategory]?, isCollapsed: Bool = true, isCollapsible: Bool = true) {
        self.categoryName = categoryName
        self.subCategories = subCategories
        self.isCollapsed = isCollapsed
        self.isCollapsible = isCollapsible
    }
}

struct SubCategory {
    var subCategoryName: String
    var items: [String]?
    var isCollapsed: Bool
    var isCollapsible: Bool
    
    init(subCategoryName: String, items: [String]?, isCollapsed: Bool = true, isCollapsible :Bool = true) {
        self.subCategoryName = subCategoryName
        self.items = items
        self.isCollapsed = isCollapsed
        self.isCollapsible = isCollapsible
    }
}

