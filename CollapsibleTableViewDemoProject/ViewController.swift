//
//  ViewController.swift
//  CollapsibleTableViewDemoProject
//
//  Created by Rukmani on 20/09/17.
//  Copyright © 2017 Rukmani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var categories = [Category]()
    let cellIdentifier = "TableViewCell"
    let headerViewIdentifier = "CollapsibleTableHeaderView"
    var subCategoryCount = 0
    var itemsCount = 0
    var section = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setUpTableView()
        setUpData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: headerViewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        
    }

    func setUpData() {
        let subCategory1 = SubCategory(subCategoryName: "Sub-Category 1", items: ["item 1", "item 2","item 3"], isCollapsed: true, isCollapsible: true)
        let subCategory2 = SubCategory(subCategoryName: "Sub-Category 2", items: ["item 1","item 2"], isCollapsed: false, isCollapsible: true)
        let subCategory3 = SubCategory(subCategoryName: "Sub-Category 3", items: ["item 1", "item 2"], isCollapsed: false, isCollapsible: true)
        let category1 = Category(categoryName: "Category 1", subCategories: [subCategory1, subCategory2, subCategory3], isCollapsed: true, isCollapsible: true)
        let category2 = Category(categoryName: "Category 2", subCategories: [subCategory1], isCollapsed: true, isCollapsible: true)
        let category3 = Category(categoryName: "Category 3", subCategories: [subCategory1, subCategory2], isCollapsed: true, isCollapsible: true)
        categories.append(category1)
        categories.append(category2)
        categories.append(category3)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        let category = categories[section]
        if category.isCollapsible {
            if category.isCollapsed {
                return 0
            } else {
                if let subCategories = category.subCategories {
                    for subCategory in subCategories {
                        if subCategory.isCollapsible {
                            if subCategory.isCollapsed {
                                rows += 1
                            } else {
                                rows += (subCategory.items!.count + 1)
                            }
                        } else {
                            rows += 1
                        }
                    }
                    return rows
                } else {
                    return 0
                }
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier) as! CollapsibleTableHeaderView
        let headerView = HeaderView()
        headerView.label.text = categories[section].categoryName
        headerView.delegate = self
        headerView.section = section
        headerView.setCollapsed(isCollapsed: categories[section].isCollapsed)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TableViewCell else {
            return UITableViewCell()
        }
        if indexPath.section != section {
            section = indexPath.section
            subCategoryCount = 0
            itemsCount = 0
        }
        if let subCategories = categories[indexPath.section].subCategories {
            if indexPath.row == 0 {
                if let items = subCategories[0].items {
                    itemsCount = items.count
                }
                cell.configureCell(title: subCategories[0].subCategoryName, isExpandable: true, tapDelegate: self)
                cell.addTapGesture()
                cell.tag = section
            } else {
                if indexPath.row > itemsCount {
                    subCategoryCount = 0
                    if let items = subCategories[subCategoryCount].items {
                        itemsCount = items.count
                    }
                    cell.configureCell(title: subCategories[subCategoryCount].subCategoryName, isExpandable: true, tapDelegate: self)
                    cell.addTapGesture()
                    cell.tag = section

                }
            }
        }
        subCategoryCount += 1
        return cell
    }
}

extension ViewController: ToggleHandler {
    func handleToggle(sender: Any, section: Int) {

        if let header = sender as? HeaderView {
            let isCollapsed = !(categories[section].isCollapsed)
            categories[section].isCollapsed = isCollapsed
            header.setCollapsed(isCollapsed: isCollapsed)
        }
        if let cell = sender as? TableViewCell {
            categories[section].subCategories?[0].isCollapsed = !((categories[section].subCategories?[0].isCollapsed)!)
        }
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
}


