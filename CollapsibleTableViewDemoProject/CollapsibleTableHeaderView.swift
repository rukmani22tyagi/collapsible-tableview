//
//  CollapsibleTableHeaderView.swift
//  CollapsibleTableViewDemoProject
//
//  Created by Rukmani on 20/09/17.
//  Copyright © 2017 Rukmani. All rights reserved.
//

import UIKit

protocol ToggleHandler {
    func handleToggle(sender: Any, section: Int)
}

class CollapsibleTableHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: UILabel!

    var delegate: ToggleHandler?
    var section: Int?

    func setCollapsed(isCollapsed: Bool) {
        
    }
    
    @IBAction func tapHandler(_ sender: UITapGestureRecognizer) {
//        self.delegate!.handleToggle(header: self, section: self.section!)
    }
    
//    var view: UIView!
//    
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//        loadView()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.loadView()
//    }
//    
//    func loadView() {
//        view = loadViewFromNib()
//        view.frame = self.bounds
//        self.addSubview(view)
//    }
//    
//    func loadViewFromNib() -> UIView {
//        let bundle = Bundle(for: type(of: self))
//        //grab nib from bundle
//        let nib = UINib(nibName: "CollapsibleTableHeaderView", bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil)[0] as! UIView
//    }
}
