//
//  HeaderView.swift
//  CollapsibleTableViewDemoProject
//
//  Created by Rukmani on 20/09/17.
//  Copyright © 2017 Rukmani. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var arrowLabel: UILabel!
    
    var view: UIView!
    var delegate: ToggleHandler?
    var section: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadView()
    }
    
    func loadView() {
        view = loadViewFromNib()
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        //grab nib from bundle
        let nib = UINib(nibName: "HeaderView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func setCollapsed(isCollapsed: Bool) {
        //rotate arrow label
        if isCollapsed {
            arrowLabel.rotate(0.0)
        } else {
            arrowLabel.rotate(CGFloat(Double.pi/2))
        }
    }
    
    @IBAction func tapHeader(_ sender: UITapGestureRecognizer) {
        self.delegate!.handleToggle(sender: self, section: self.section!)
    }
}
